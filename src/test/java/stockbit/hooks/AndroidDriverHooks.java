package stockbit.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import stockbit.android_driver.AndroidDriverInstance;

import java.net.MalformedURLException;

import static stockbit.utils.Constans.ELEMENTS;
import static stockbit.utils.Utils.loadElementProperties;

public class AndroidDriverHooks {
    @Before(value = "@Android")
    public void initializeAndroidDriver() throws MalformedURLException {
        AndroidDriverInstance.initialize();
        loadElementProperties(ELEMENTS);
    }

    @After(value = "@Android")
    public void quitDriver(Scenario scenario) {
        boolean testStatus = scenario.isFailed(); // True or false
        try {
            if (testStatus == true) {
                final byte[] failedImage = ((TakesScreenshot) AndroidDriverInstance.androidDriver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(failedImage, "image/png", "Failed Screenshot");
            }
        } catch (Exception error) {
            error.printStackTrace();
        } finally {
            AndroidDriverInstance.quit();
        }
    }
}

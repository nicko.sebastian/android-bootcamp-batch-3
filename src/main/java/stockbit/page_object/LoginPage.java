package stockbit.page_object;

import org.openqa.selenium.By;

import static stockbit.utils.Utils.env;

public class LoginPage extends BasePage {
    public void isOnBoardingPage() {
        assertIsDisplayed("ICON_STOCKBIT");
    }

    public void tapLogin() {
        tap("BUTTON_LOGIN_ENTRY_POINT");
    }

    public void inputUsername(String username) {
        typeOn("FIELD_USERNAME", env(username));
    }

    public void inputPassword(String password) {
        typeOn("FIELD_PASSWORD", env(password));
    }

    public void tapLoginButton() throws InterruptedException {
        tap("BUTTON_LOGIN");
        Thread.sleep(5000);
    }

    public void tapSkipBiometricPopup() {
        tap(By.id("com.stockbit.android:id/btn_smart_login_skip"));
    }

    public void tapSkipAvatarPopup() {
        tap(By.id("com.stockbit.android:id/btn_skip_choose_avatar"));
    }

    public void isWatchlistPage() {
        tapSkipBiometricPopup();
        tapSkipAvatarPopup();
        assertIsDisplayed(By.xpath("//android.widget.TextView[@resource-id='com.stockbit.android:id/tv_name' and @text='All Watchlist']"));
    }
}

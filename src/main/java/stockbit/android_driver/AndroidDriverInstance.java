package stockbit.android_driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidDriverInstance {
    public static AndroidDriver androidDriver;

    public static void initialize() throws MalformedURLException {
        UiAutomator2Options caps = new UiAutomator2Options();

        caps.setPlatformName("Android");
        caps.setPlatformVersion("12");
        caps.setUdid("emulator-5554");
        caps.setApp("/Users/nickosebastian/Documents/android APK/2.20.2-rc3(10800).apk");
        caps.setAppWaitActivity("*");

        try {
            androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723"), caps);
        } catch (MalformedURLException errorMessage) {
            throw new RuntimeException(errorMessage);
        }
    }

    public static void quit() {
        androidDriver.quit();
    }
}
